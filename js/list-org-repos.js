import { Octokit } from './dependency-octokit.js';

const ListOrgRepos = class extends HTMLElement {
	async connectedCallback () {
		this.org = this.getAttribute('org') || null
		this.octokit = new Octokit();
		this.repos = await this.getOrgRepos() || []
		this.render()
	}
	async getOrgRepos() {
		let repos
		try {
			repos = await this.octokit.request(`GET /orgs/${this.org}/repos`, {
				org: 'org'
			}).then(res => {
				if (res.status === 200) {
					return res.data
				} else {
					return []
				}
			})
		} catch (error) {
			console.log(`Error fetching the repo for org:`, this.org, error)
			repos = []
		}
		return repos
	}
	render() {
		let $repos = document.createElement('main')
		if (!this.repos) {
			$repos.innerText = `No repos; ${this.repos.length}`
		} else {
			this.repos.forEach(repo => {
				const $repo = document.createElement('article')
				const $repoLink = document.createElement('a')
				$repoLink.innerText = repo.name
				$repoLink.href = repo.html_url
				$repo.append($repoLink)
				$repos.append($repo)
			})
		}
		this.append($repos)
	}
}

export default ListOrgRepos
