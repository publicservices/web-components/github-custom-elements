Web components for Github API

# `<list-org-repos>`

Lists all repositories for a github organisation.

Params:
- `org` [string], the organisation's `slug` (username, as in `github.com/{org}`)

Docs: https://docs.github.com/en/rest/reference/repos#list-organization-repositories
